# « Hello, World! » sur Google App Engine avec Terraform

## Préparation

1. Créer un *bucket* sur *Cloud Storage* pour stocker l'état de Terraform. Indiquer son nom dans le fichier `infra/providers.tf` ;
2. Activer l'application App Engine ;
3. Créer un fichier `[XXX].tfvars` avec les informations nécessaires (voir `infra/variables.tf`).
