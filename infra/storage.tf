resource "google_storage_bucket" "whatever_storage" {
  name = "whatever-storage-${var.gcp_project_id}-${random_id.suffix.hex}"
  location = var.gcp_region
  force_destroy = true
}
