terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "~> 5.10"
    }
  }

  backend "gcs" {
    bucket = "hw-gae-tf-sandbox-antoine"
    prefix = "hw-gae-terraform/state"
  }
}

provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_region
  zone    = var.gcp_zone
}
