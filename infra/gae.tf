resource "random_id" "suffix" {
  byte_length = 8
}

data "archive_file" "hello_world_app" {
  type = "zip"
  source_dir = "../app"
  output_path = "../hw-app.zip"
}

resource "google_storage_bucket" "gae_apps_storage" {
  name = "gae-apps-storage-${var.gcp_project_id}-${random_id.suffix.hex}"
  location = var.gcp_region
  force_destroy = true
  versioning {
    enabled = true
  }
}

resource "google_storage_bucket_object" "hello_world_app" {
  name = "hw-app.zip"
  source = data.archive_file.hello_world_app.output_path
  bucket = google_storage_bucket.gae_apps_storage.name
}

resource "google_app_engine_application_url_dispatch_rules" "hw-app-dispatch-rules" {
  dispatch_rules {
    domain = "*"
    path = "/*"
    service = "default"
  }

  depends_on = [google_app_engine_standard_app_version.hw_latest_version]
}

resource "google_app_engine_standard_app_version" "hw_latest_version" {
  version_id = var.deployment_version
  service = "default"
  runtime = "go122"

  entrypoint {
    shell = "go run helloworld.go"
  }

  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.gae_apps_storage.name}/${google_storage_bucket_object.hello_world_app.name}"
    }
  }

  instance_class = "F1"

  automatic_scaling {
    max_concurrent_requests = 10
    min_idle_instances      = 1
    max_idle_instances      = 3
    min_pending_latency     = "1s"
    max_pending_latency     = "5s"
    standard_scheduler_settings {
      target_cpu_utilization        = 0.5
      target_throughput_utilization = 0.75
      min_instances                 = 0
      max_instances                 = 4
    }
  }
  noop_on_destroy = true
  delete_service_on_destroy = true
}
